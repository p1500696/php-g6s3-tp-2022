<?php

	ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);

    $target = isset($_GET["target"]) ? $_GET["target"] : "films"; 

    require_once("controller/c_" . $target . ".php");

?>