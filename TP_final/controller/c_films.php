<?
	require_once("libs/functions.php");
	require_once("model/m_films.php");
	require_once("view/v_films.php");

	if (isset($_SESSION['username']))
	{
		if (isset($_GET['drop']) && isset($_GET['id']))	{
			if ($_GET['drop'])
			{
				drop_movie($_GET['id']);
				header('Location: index.php');
			}
		}

		if (isset($_GET['session']))
		{
			if ($_GET['session'] == 'destroy')
			{
				session_destroy();
				header('Location: index.php');
			}
		}
	}

?>