<?php

    require_once("libs/functions.php");
    require_once("model/m_signin.php");
    require_once("view/v_signin.php");

    if (isset($_POST['signin']))
    {
        if (isset($_POST['username']) && isset($_POST['password']) && isset($_POST['email']))
        {
            if (check_exist($_POST['username']))
            {
?>
                <p class="erreur">Le nom d'utilisateur est déjà utilisé !</p>
<?
            }
            else
            {
                create_account($_POST['username'], $_POST['password'], $_POST['email']);
?>
                <p class="success">Vous pouvez désormais vous <a href="index.php?target=login">connecter</a> !</p>
<?
            }
        }
    }

?>