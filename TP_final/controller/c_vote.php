<?php

    require_once('libs/functions.php');
    require_once('model/m_vote.php');
    require_once('view/v_vote.php');

    if (isset($_POST['valid_vote']) && isset($_POST['value_vote']))
    {
        if (is_numeric($_POST['value_vote']) && $_POST['value_vote'] >= 0 && $_POST['value_vote'] <= 10)
        {
            vote($data['id'], get_userid($_SESSION['username']), $_POST['value_vote']);
            header('Location: index.php');
        }
    }

?>