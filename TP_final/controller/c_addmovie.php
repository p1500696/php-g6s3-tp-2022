<?
	require_once("libs/functions.php");
	require_once("model/m_addmovie.php");
	require_once("view/v_addmovie.php");

	if (isset($_POST['nom']) && isset($_POST['annee']) && isset($_POST['addmovie_valid']))
	{
		add_movie($_POST['nom'], $_POST['annee']);
		header('Location: index.php');
	}


?>