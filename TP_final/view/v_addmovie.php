<? session_start(); ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="style_addmovie.css">
	<link rel="stylesheet" type="text/css" href="style.css">
	<title>Médiathèque | Add movie</title>
</head>
<body>
	<? require_once("view/header.php"); ?>

	<form method="POST" action="index.php?target=addmovie" class="addmovie">
		<div class="g-input">
		  <input type="text" id="nom" name="nom" placeholder=" ">
		  <label for="nom">Nom du film</label>
		</div>
		<div class="g-input">
		  <input type="number" id="annee" name="annee" placeholder=" ">
		  <label for="annee">Année de sortie</label>
		</div>
		<button type="submit" name="addmovie_valid">Envoyer</button>
	</form>

</body>
</html>