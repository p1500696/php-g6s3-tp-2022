<? 
	session_start();
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" href="style.css">
	<link rel="stylesheet" href="style_login.css">
	<title>Médiathèque | SIGN IN</title>
</head>
<body>

	<?php include_once('view/header.php'); ?>

	<?
		if (!isset($_SESSION['username']))
		{
	?>
	<form action="index.php?target=signin" method="post">
		<label for="username">Username</label>
		<input type="text" name="username" id="username" require>
		<label for="password">Password</label>
		<input type="password" name="password" id="password" require>
        <label for="email">Mail</label>
        <input type="email" name="email" id="email" placeholder="jeandupont@example.fr" require>
		<input type="submit" name="signin" value="Valider">
	</form>
	<? 
		}
		elseif ($_SESSION['username'] != 'visitor')
		{
			echo "Bienvenue " . $_SESSION['username'];
		}
	?>

</body>
</html>