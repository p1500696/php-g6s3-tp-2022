<? session_start(); ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="style_addmovie.css">
	<link rel="stylesheet" type="text/css" href="style.css">
	<title>Médiathèque | Vote</title>
</head>
<body>
	<? require_once("view/header.php"); ?>
    <?php
        if (isset($_SESSION['username']))
        {
            if (!isset($_POST['valid_vote']))
            {
    ?>
                <form action="index.php?target=vote&id=<? echo $data['id']; ?>" method="POST">

                <h1><?php echo $data['nom']; ?></h1>
                <p>Quelle note voulez-vous attribuer à ce film (0 à 10) ?</p>
                <input type="number" step="0.1" min="0" max="10" name="value_vote" require>
                <input type="submit" name="valid_vote" value="Voter">
        
                </form>
    <?php
            }
            else
            {
    ?>
                <p class="erreur">Vous avez déjà voté ! Revenez dans ...</p>
    <?php
            }
        }
    ?>

</body>
</html>