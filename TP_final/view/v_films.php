<? session_start() ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="../style_home.css">
	<link rel="stylesheet" type="text/css" href="../style.css">
	<title>Médiathèque</title>
</head>
<body>
	<? require_once("view/header.php"); ?>
	<?

		if (isset($_SESSION['username']))
		{
	?>
		<a href="index.php?target=addmovie" class="add"><span class="material-icons">add</span></a>
	<?

		}

	?>
	<table class="rwd-table">
		<tr class="header">
			<?php
				foreach($colName as $row)
				{
			?>
				<th>
			<?
					echo $row;
			?>
					<a href="index.php?orderBy=<? echo $row ?>&orderType=ASC">
						<span class="material-icons">
							arrow_drop_up
						</span>
					</a>
					<a href="index.php?orderBy=<? echo $row ?>&orderType=DESC">
						<span class="material-icons">
							arrow_drop_down
						</span>
					</a>
				</th>
			<?php
				}
			?>
		</tr>
		<? 
			foreach($data as $row)
			{
		?>
		<tr>
			<td>
				<?
					echo $row['id'];
					if (isset($_SESSION['username']) && $_SESSION['username'] == 'admin')
					{
				?>
						<a href="index.php?target=films&drop=true&id=<? echo $row['id']; ?>"><span class="material-icons">delete_forever</span>
				<?
					}
				?>
				</td>
			<td>
				<a style="float: left;" href="index.php?target=details&id=<? echo $row['id']; ?>"><span class="material-icons">loupe</span></a>
				<? echo $row['nom']; ?>
			</td>
			<td>
				<? echo $row['annee']; ?>
			</td>
			<td>
				<? echo $row['score']; ?>
				<? if (isset($_SESSION['username']))
					{
				?>
					<a href="index.php?target=vote&id=<? echo $row['id']; ?>">Voter</a>
				<?
					}
				?>
			</td>
			<td><? echo $row['nbVotants']; ?></td>
		</tr>
		<?
			}
		?>

	</table>
</body>
</html>