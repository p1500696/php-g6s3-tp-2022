<? session_start(); ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="style.css">
    <link rel="stylesheet" type="text/css" href="style_details.php">
	<title>Médiathèque | Détails</title>
</head>
<body>
	<? require_once("view/header.php"); ?>
    <section class="details">
        <h1>Détails du film <? echo $details['nom'] ?></h1>
        <ul>
            <li>Ce film est sorti durant l'année <? echo $details['annee']; ?></li>
            <li><? echo $details['nbVotants']; ?> personnes ont vôté pour ce film</li>
            <li>La note moyenne de ce film est <? echo $details['score']; ?>/10</li>
        </ul>
    </section>

</body>
</html>