<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" href="style.css">
	<link rel="stylesheet" href="style_profil.css">
	<title>Médiathèque | PROFIL</title>
</head>
<body>

	<?php include_once('view/header.php'); ?>
	<section class="table_vote">
	<?
		if (isset($_SESSION['username']))
		{
	?>
        <h1>Profile</h1>
        <h2>Welcome <?php echo ucfirst($_SESSION['username']); ?></h2>
        <p>Here are your last votes</p>
        <table>
                <?
                    foreach($data_vote as $row)
                    {
                ?>
						<tr>
							<td><? echo get_movie($row['id_film'])['nom']; ?></td>
							<td><? echo $row['date_vote']; ?></td>
						</tr>
                <?
                    }
                ?>
                
	<? 
		}
		elseif (isset($_SESSION['username']))
		{
			header('Location: index.php?target=login');
		}
	?>
	</section>

</body>
</html>