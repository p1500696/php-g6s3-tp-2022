<? 
	session_start();
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" href="style.css">
	<link rel="stylesheet" href="style_login.css">
	<title>Médiathèque | LOGIN</title>
</head>
<body>

	<?php include_once('view/header.php'); ?>

	<?
		if (!isset($_SESSION['username']))
		{
	?>
		<form action="index.php?target=login" method="post">
			<label for="username">Username</label>
			<input type="text" name="username" id="username">
			<label for="password">Password</label>
			<input type="password" name="password" id="password">
			<input type="submit" name="login" value="Login">
		</form>
		<p>OU</p>
		<a href="index.php?target=signin">Créer un compte</a>
	<? 
		}
		elseif (isset($_SESSION['username']))
		{
			header('Location: index.php?target=profil');
		}
	?>

</body>
</html>