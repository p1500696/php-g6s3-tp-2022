<?

	function connect_db()
	{
		$db = null;
		try
		{
			$db = new PDO(
				'mysql:host=iutdoua-web.univ-lyon1.fr;dbname=p1500696;charset=utf8', 
				'p1500696',
				'603794'
			);
		}
		catch (Exception $e)
		{
			die('Erreurkkk  ' . $e->getMessage());
		}

		return $db;
	}

	// Fonction pour supprimer des votes en même temps qu'un film pour ne pas avoir de vote relié à un film inexistant
	function drop_vote($id_film)
	{
		$db = connect_db();
		$stmt = $db->prepare('DELETE FROM vote WHERE id_film = ?');
		$stmt->execute([$id_film]);
		$stmt->closeCursor();
	}

	function drop_movie($id)
	{
		drop_vote($id);
		$db = connect_db();
		$stmt = $db->prepare('DELETE FROM film WHERE id=?');
		$stmt->execute([$id]);
		$stmt->closeCursor();
	}

	function check_login($username, $password)
    {
        $db = connect_db();
        $stmt = $db->prepare("SELECT * FROM users WHERE login=? AND password=?");
        $stmt->execute([$username, $password]);
        $data = $stmt->fetch();
        $stmt->closeCursor();

        return $data;
    }

	function check_exist($username)
	{
		$db = connect_db();
		$stmt = $db->prepare("SELECT * FROM users WHERE login=?");
		$stmt->execute([$username]);
		$data = $stmt->fetch();
		$stmt->closeCursor();

		return $data;
	}

	function create_account($username, $password, $email)
	{
		$db = connect_db();
		$stmt = $db->prepare("INSERT INTO users VALUES (0, ?, ?, ?)");
		$stmt->execute([$username, $password, $email]);
		$stmt->closeCursor();
	}

	function get_movie($id)
	{
		$db = connect_db();
		$stmt = $db->prepare("SELECT * FROM film WHERE id=?");
		$stmt->execute([$id]);
		$data = $stmt->fetch();
		$stmt->closeCursor();

		return $data;
	}

	function vote($id_film, $id_user, $note)
	{
		$date = new DateTime('now', new DateTimeZone('Europe/Paris'));
		echo $id_user;
		$date = $date->format('Y-m-d H:i:s');
		$db = connect_db();
		$stmt_add_vote = $db->prepare("INSERT INTO vote VALUES (0, ?, ?, ?, ?)");
		$stmt_update_nbvotants = $db->prepare("UPDATE film SET nbVotants=nbVotants+1 WHERE id=?");
		$stmt_update_score = $db->prepare("UPDATE film SET score=ROUND(score+(?-score)/nbVotants,1) WHERE id=?");
		$stmt_add_vote->execute([
			intval($id_film),
			intval($id_user),
			floatval($note),
			$date,
		]);
		$stmt_update_nbvotants->execute([intval($id_film)]);
		$stmt_update_score->execute([floatval($note), intval($id_film)]);
		$stmt_add_vote->closeCursor();
		$stmt_update_nbvotants->closeCursor();
		$stmt_update_score->closeCursor();
	}

	function get_userid($username)
	{
		$db = connect_db();
		$stmt = $db->prepare("SELECT id FROM users WHERE login=?");
		$stmt->execute([$username]);
		$data = $stmt->fetch();
		$stmt->closeCursor();
		return $data['id'];
	}

	function get_vote($username)
	{
		$db = connect_db();
		$stmt = $db->prepare("SELECT * FROM vote WHERE id_user=(SELECT id from users WHERE login=?)");
		$stmt->execute([$username]);
		$data = $stmt->fetchAll();
		$stmt->closeCursor();

		return $data;
	}

	function get_colname($tablename)
	{
		$queryCol = $db->prepare("DESCRIBE ?");
		$queryCol->execute([$tablename]);
		$colName = $queryCol->fetchAll(PDO::FETCH_COLUMN);
		$queryCol->closeCursor();

		return $colName;
	}

?>