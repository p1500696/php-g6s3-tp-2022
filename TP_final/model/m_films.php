<?php 

	$db = connect_db();

	$orderBy = "id";
	$orderType = "ASC";

	if (isset($_GET['orderType']) && ($_GET['orderType'] == "ASC" || $_GET['orderType'] == "DESC"))
	{
		$orderType = $_GET['orderType'];
	}
	
	$queryCol = $db->prepare("DESCRIBE film");
	$queryCol->execute();
	$colName = $queryCol->fetchAll(PDO::FETCH_COLUMN);

	if (isset($_GET['orderBy']))
	{	
		if (in_array($_GET['orderBy'], $colName))
		{
			$orderBy = $_GET['orderBy'];
		}
		else
		{
			die("La colonne n'existe pas");
		}
	}

	$filmStatement = $db->prepare('SELECT * FROM film ORDER BY ' . $orderBy . ' ' . $orderType);
	$filmStatement->execute();
	$data = $filmStatement->fetchAll();

?>