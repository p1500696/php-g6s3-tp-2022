<?php

	function CalcImpots($nbenfants, $isMaried, $revenu_net)
	{
		// On calcule le nombre de part

		if ($isMaried)
		{
			$nbparts = $nbenfants + 1;
		}
		else
		{
			$nbparts = $nbenfants;
		}

		// On calcule le revenu par part et on modifie la variabke $impots en conséquence
		$revenu_par_part = $revenu_net / $nbparts;

		switch(true)
		{
			case $revenu_par_part <= 9963:
				break;
			case $revenu_par_part <= 27518:
				$impots = ($revenu_net * 0.14) - 1394.96;
				break;
			case $revenu_par_part <= 73778:
				$impots = ($revenu_net * 0.30) - 5798;
				break;
			case $revenu_par_part <= 156243:
				$impots = ($revenu_net * 0.41) - 13910.69;
				break;
			case $revenu_par_part >= 156244:
				$impots = ($revenu_net * 0.45) - 20163.45;
				break;
		}

		return $impots;

	}

?>