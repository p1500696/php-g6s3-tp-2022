<?php
	// On récupère model.php et controller.php
	require_once('model.php');	// Pour récupérer la variable $impots
	require_once('controller.php');		// Pour la fonction CalcImpots()

	// On lance la fonction CalcImpots qui modifie la variable $impots de model.php
	$impots = CalcImpots($_GET['nbenfants'], $_GET['isMaried'], $_GET['revenu_net']);

	// On 'redirige' vers index.php
	require_once('index.php');
?>